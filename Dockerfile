FROM python:3.13.2
WORKDIR /app
COPY requirements.txt /app
RUN pip install --no-cache-dir -r requirements.txt
COPY *.py /app
CMD ["python", "-u", "hacker_news.py"]
