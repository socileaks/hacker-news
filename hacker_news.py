#!/usr/bin/env python3
''' Hacker News crawler '''

import html
import json
import random
import re
import textwrap
from datetime import datetime, timezone
from time import sleep, time
from urllib.request import Request, urlopen

import requests
from linkpreview import Link, LinkGrabber, LinkPreview
from pymongo import MongoClient
from rich import inspect
from rich.console import Console


def prrint(text=''):
    ''' Rich print function '''
    console = Console(soft_wrap=True)
    console.print(text)


def prrint_exception():
    ''' Rich print traceback '''
    console = Console(soft_wrap=True)
    console.print_exception()


def prrint_json(dictionary):
    ''' Convert a dictionary to JSON '''
    prrint(json.dumps(dictionary, ensure_ascii=False))


def hacker_news(route):
    ''' Requests wrapper for Hacker News '''
    url = f'https://hacker-news.firebaseio.com/v0/{route}.json'
    return requests.get(url, timeout=10).json()


def timestamp_to_iso(timestamp):
    ''' Convert Unix timestamp to ISO human-readable format '''
    return datetime.fromtimestamp(timestamp).replace(
        tzinfo=timezone.utc).isoformat()


def main(images_cache):
    ''' Main function '''

    # Fetch Hacker News best posts
    posts = hacker_news('beststories')

    max_posts = 20

    images_cache_new = {}
    posts_final = []
    siblings = []
    for post_id in posts[:max_posts]:
        post_url = f'https://news.ycombinator.com/item?id={post_id}'

        post = hacker_news(f'item/{post_id}')

        comments = post['kids'] if 'kids' in post else []
        siblings.append(post['descendants'] / (time() - post['time'] + 1))

        # Fetch thumbnail for link
        fetch_image_from_cache = False
        image = ''
        if 'url' in post:
            link = post['url']
            if link in images_cache and (images_cache[link]
                                         or random.random() < 0.90):
                fetch_image_from_cache = True
                image = images_cache[link]
                if image:
                    prrint_json({'cached': image})
                else:
                    prrint_json({'cached': None, 'url': link, 'level': 'warn'})
            else:
                try:
                    headers = {}
                    if link.startswith('https://x.com/') or link.startswith(
                            'https://twitter.com/'):
                        headers = {'User-Agent': 'Facebot'}
                    grabber = LinkGrabber(maxsize=3 * 1048576)
                    content, url = grabber.get_content(link, headers=headers)
                    preview = LinkPreview(Link(url, content))
                    try:
                        image = preview.absolute_image
                    except AttributeError:
                        if 'url' in preview.image:
                            image = preview.image['url']
                        else:
                            inspect(preview)
                except Exception as exception:
                    prrint(exception)
        else:
            link = post_url

        post = {
            'author': post['by'],
            'stars': post['score'],
            'post': post_url,
            'link': link,
            'image': image,
            'title': html.escape(post['title']),
            'date': timestamp_to_iso(post['time'])
        }

        if not fetch_image_from_cache:
            if image:
                image = re.sub('^http://', 'https://', image)
                image = re.sub(' ', '%20', image)
                request = Request(
                    image,
                    headers={
                        'User-Agent':
                        'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:95.0)'
                    })
                is_image = False
                try:
                    with urlopen(request, timeout=10) as download:
                        if download.getcode() == 200:
                            try:
                                data = download.read().decode()
                                if (not re.search(r'<html\b', data, re.I)
                                        and re.search(r'<svg\b', data, re.I)):
                                    is_image = True
                            except UnicodeDecodeError:
                                is_image = True
                except Exception as exception:
                    prrint(exception)
                if is_image:
                    prrint_json({'thumbnail': image, 'level': 'info'})
                    post['image'] = image
                else:
                    prrint_json({'problematic': image, 'level': 'warn'})
                    image = ''
            else:
                prrint_json({'thumbnail': None, 'url': link, 'level': 'warn'})
        if not image:
            del post['image']
        images_cache_new[link] = image

        posts_final.append(post)

        if comments:
            posts_final.append({
                'post_url': post_url,
                'comment_url': f'item/{comments[0]}',
                'siblings': siblings[-1]
            })

    # Keep only best comments
    max_comments = max_posts // 3
    if len(siblings) > max_comments:
        siblings.sort()
        threshold = siblings[-max_comments]
        posts_final = [
            p for p in posts_final
            if 'siblings' not in p or p['siblings'] >= threshold
        ]

    # Fetch comments
    for i, post in enumerate(posts_final):
        if 'comment_url' in post:
            comment = hacker_news(post['comment_url'])

            text = comment['text']

            # Replace anchor tags by empty string
            text = re.sub('<a [^>]+>', '', text)
            text = re.sub('</a>', '', text)

            # Italicize quoted text
            text = re.sub('(((^|<p>)&gt; ?(.+?))+)(<p>|$)', '<i>\\1</i>\\5',
                          text)

            # Remove HTML tags apart from <i>
            text = re.sub('</?([^i]|[^/][^>]+)>', ' ', text)

            # Trim comment
            max_comment_len = 280
            if len(text) > max_comment_len:
                text = textwrap.shorten(text,
                                        width=max_comment_len,
                                        placeholder='')

                # Close dangling <i> tags
                for _ in range(text.count('<i>') - text.count('</i>')):
                    text = text + '</i>'

                # Remove unneeded punctuation and add ellipsis in the end
                text = re.sub(r'[^]})>;\w]*((</i>)*)$', r'\1…', text, count=1)

            posts_final[i] = {
                'author': comment['by'],
                'post': post['post_url'],
                'comment': text,
                'date': timestamp_to_iso(comment['time'])
            }

    # Store to database
    with MongoClient('mongodb://db') as client:
        database = client['db']
        news_collection = database['news']
        news_collection.update_one(filter={'_id': 'hacker-news'},
                                   update={
                                       '$set': {
                                           '_id': 'hacker-news',
                                           'name': 'Hacker News',
                                           'news': posts_final
                                       }
                                   },
                                   upsert=True)

    return images_cache_new


if __name__ == '__main__':
    img_cache = {}
    while True:
        try:
            img_cache = main(img_cache)
        except Exception as exc:
            prrint_exception()
            prrint_json({'level': 'error', 'message': repr(exc)})
        SLEEP_MINS = 10
        sleep(SLEEP_MINS * 60)
        prrint()
